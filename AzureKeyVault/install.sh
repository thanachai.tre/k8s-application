#!/bin/bash

helm repo add csi-secrets-store-provider-azure https://azure.github.io/secrets-store-csi-driver-provider-azure/charts
helm repo update
helm install csi csi-secrets-store-provider-azure/csi-secrets-store-provider-azure