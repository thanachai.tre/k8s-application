# How to use Azure KeyVault to store Kubernetes secret

## Step 
-   Install CSI Driver via helm chart via run [install.sh](https://gitlab.com/thanachai.tre/thanachai-repo/-/blob/main/K8s/AzureKeyVault/install.sh)
-   Create Azure KeyVault and Secret
-   Create and assign Service Principal authen to Azure KeyVault (GET policy)
-   Create K8s secret with Service Principal credential 
```bash
kubectl create secret generic secrets-store-creds --from-literal clientid=${SERVICE_PRINCIPAL_CLIENT_ID} --from-literal clientsecret=${SERVICE_PRINCIPAL_CLIENT_SECRET}
kubectl label secret secrets-store-creds secrets-store.csi.k8s.io/used=true
```
-   Create Secret Provider Class [SecretProviderClass](https://gitlab.com/thanachai.tre/thanachai-repo/-/blob/main/K8s/AzureKeyVault/SecretProviderClass.yaml)
-   Deploy application to use it

# Remark 
-   Application deployment, Secret with Service Principal credential and Secret Provider Class must locate in same Namespace. 