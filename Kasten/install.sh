#!/bin/bash

kubectl create ns kasten-io
helm repo add kasten https://charts.kasten.io/
helm repo update
helm install kasten -n kasten-io kasten/k10 -f values.yaml