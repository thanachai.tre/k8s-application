# Kasten Backup 
Kasten is a powerful data management platform that helps you protect, secure, and manage your Kubernetes applications and persistent data. With Kasten, you can easily backup, restore, and migrate your containerized applications, ensuring that your data is safe, and your applications run smoothly in your Kubernetes environment.

## Prerequisite
- Run **primer tool** to validate settings meet Kasten requirements.
```bash
curl https://docs.kasten.io/tools/k10_primer.sh | bash
```
- Ensure VolumeSnapshotClass deployed to your Kubernetes cluster with annotation 
```bash
k10.kasten.io/is-snapshot-class: "true"
```
- If VolumeSnapshotClass didn't deployed to must deploy first
```yaml
kind: VolumeSnapshotClass
apiVersion: snapshot.storage.k8s.io/v1
metadata:
  name: <Class Name>
  annotations:
    k10.kasten.io/is-snapshot-class: "true"
driver: <CSI Driver>
deletionPolicy: Delete
```
- AStorageClass can be annotated with the following below forAll volumes created with this StorageClass will be snapshotted by the specified VolumeSnapshotClass.
```bash
kubectl annotate storageclass ${SC_NAME} \
    k10.kasten.io/volume-snapshot-class=${VSC_NAME}
```

## Install 
- Custome Helm [values](values.yaml) file.
- Install via [install.sh](./install.sh)
```bash
chmod +x install.sh
./install.sh
```
