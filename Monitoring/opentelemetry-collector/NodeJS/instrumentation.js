/*instrumentation.js*/
opentelemetryURL = 'http://collector.kanomnutt.site'
const { diag, DiagConsoleLogger, DiagLogLevel } = require('@opentelemetry/api');
diag.setLogger(new DiagConsoleLogger(), DiagLogLevel.ERROR);
const opentelemetry = require('@opentelemetry/sdk-node');
const {
  getNodeAutoInstrumentations,
} = require('@opentelemetry/auto-instrumentations-node');
const {
  OTLPTraceExporter,
} = require('@opentelemetry/exporter-trace-otlp-http');
const {
  OTLPMetricExporter,
} = require('@opentelemetry/exporter-metrics-otlp-http');
const { PeriodicExportingMetricReader } = require('@opentelemetry/sdk-metrics');

const sdk = new opentelemetry.NodeSDK({
  traceExporter: new OTLPTraceExporter({
    // optional - default url is http://localhost:4318/v1/traces
    // url: 'http://collector.kanomnutt.site:80/v1/traces',
    url: `${opentelemetryURL}/v1/traces`,
    // optional - collection of custom headers to be sent with each request, empty by default
    headers: {
      'Content-Type': 'application/json',
    },
  }),
  metricReader: new PeriodicExportingMetricReader({
    exporter: new OTLPMetricExporter({
      // url: 'http://collector.kanomnutt.site:80/v1/metrics', // url is optional and can be omitted - default is http://localhost:4318/v1/metrics
      url: `${opentelemetryURL}/v1/metrics`,
      headers: {}, // an optional object containing custom headers to be sent with each request
      concurrencyLimit: 1, // an optional limit on pending requests
    }),
  }),
  instrumentations: [getNodeAutoInstrumentations()],
  serviceName: "NodeJS OTEL"
});
sdk.start();

