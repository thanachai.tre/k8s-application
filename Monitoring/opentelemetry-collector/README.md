# OpenTelemetry Tracing Example

## Example send Tracing from Express NodeJS to OpenTelemetry Collector in Kubernetes

## Prerequisite
    - NodeJS
    - OpenTelemetry Collector
    - Grafana Cloud Tempo
## Step
1. Install OpenTelemetry Collector and send tracing data to Grafana cloud Tempo on Kubernetes platform.
    - Change mode in [values.yaml](values.yaml#L9)
    - Create OTLP exporter configuration to send tracing data to Grafana cloud Temp [values.yaml](values.yaml#L89-92)
    ```sh
    config:
      exporters:
        otlp:
          endpoint: <Grafana Cloud Tempo URL>
          headers:
            authorization: Basic <username:password in base64 encode>
    ```
    - Configure trace pipeline to use OTLP exporter. [values.yaml](values.yaml#L140-142)
    ```sh
    pipelines:
      traces:
        exporters:
          - otlp
    ```
    - Create Ingress Configuration [values.yaml](values.yaml#L417-430)
    - Install OpenTelemetry Collector via helm.
    ```sh
    helm repo add open-telemetry https://open-telemetry.github.io/opentelemetry-helm-charts
    helm install -n <namespace> <release name> open-telemetry/opentelemetry-collector -f values.yaml
    ```
2. Running Express in NodeJS
    - Install dependency NodeJS packages.
    ```sh
    npm install
    ```
    - Change Opentelemetry Collector URL [instrumentation.js](./NodeJS/instrumentation.js#L2)
    - Change Service Name [instrumentation.js](./NodeJS/instrumentation.js#L36)
    - Run Express
    ```sh
    node --require ./instrumentation.js ./app.js
    ```
