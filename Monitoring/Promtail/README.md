# Install Promtail on K8s.

## 1. Install via YAML file as a Daemonset.
- Add Loki Endpoint to [clients.url](promtail-ds.yaml#L62) in Secret configuration
- Add list a Namespaces don't need to [pipeline_stages.drop](promtail-ds.yaml#L76-78)
- (Optional) Add Toleration or Affinity to Daemonset.
- Apply [YAML](promtail-ds.yaml)
```sh
kubectl apply -f promtail-ds.yaml -n <namespace>
```
## 2. Install via Helm Chart values.
- Add Loki Helm Chart repo
```sh
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```
- Edit [values.yaml](values.yaml)
- Install Helm Chart
```sh
helm install promtail -n <namespace> grafana/promtail -f values.yaml
```
