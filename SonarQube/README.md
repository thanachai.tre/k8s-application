# Get Started with SonarQube

## Install SonarQube on Kubernetes platform.
- Install via Helm.
```sh
helm repo add sonarqube https://SonarSource.github.io/helm-chart-sonarqube
helm repo update
kubectl create namespace sonarqube
```
- Edit Helm chart values and install.
```sh
helm upgrade --install -n sonarqube sonarqube sonarqube/sonarqube -f values.yaml
```
## If running SonarQube server in HTTPS, you need to configure environment variable SonarQube Scanner.

- Get/Export SonarQuber server certificate in format .cer or .der.
- Use keytool command.
```sh
keytool -importcert -file <certificate file> -keystore <keystore name>.keystore -alias "Keystore alias name"
# After run above command, it will ask a keystore password enter it.
```
- On SonarQube Scanner config environment variable.
```sh 
SONAR_SCANNER_OPTS=-Djavax.net.ssl.trustStore=<keystorefile from above step> -Djavax.net.ssl.trustStorePassword=<keystorefile password from above step>
```
- Try to run SonarQube Scanner.