#!/bin/bash

kubectl create secret -n kafka docker-registry secret-docker --docker-server="<Registry Server URL>" --docker-username="<User for login to registry server>"--docker-password="<Password or token>
# Create Secrect for username
kubectl apply -n kafka -f connect-user.yml
kubectl apply -n kafka -f connector-user.yaml
sleep 5
kubectl apply -n kafka -f kafka-connect.yml
