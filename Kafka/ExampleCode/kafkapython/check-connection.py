from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import KafkaError
from kafka.admin import KafkaAdminClient, NewTopic

def test_kafka_connection(bootstrap_servers, username, password):
    try:
        # Configure SASL authentication
        sasl_mechanism = "SCRAM-SHA-512"
        sasl_plain = f"{username}:{password}"
        security_protocol = "SASL_SSL"

        # Test connection using KafkaAdminClient
        admin_client = KafkaAdminClient(
            bootstrap_servers=bootstrap_servers,
            sasl_mechanism=sasl_mechanism,
            sasl_plain_username=username,
            sasl_plain_password=password,
            security_protocol=security_protocol,
        )
        topic_metadata = admin_client.list_topics()
        topics = topic_metadata
        print("Connection to Kafka cluster successful!")
        print("Available topics:")
        for topic in topics:
            print(topic)

    except KafkaError as e:
        print(f"Failed to connect to Kafka cluster: {str(e)}")
        
if __name__ == "__main__":
    # Specify the bootstrap servers and authentication details of your Kafka cluster
    bootstrap_servers = 'kafka-external.kanomnutt.site:443'
    username = 'kafdrop-user'
    password = 'HXO3fMLo5kr0uGPyBgS7dTS3ht9CmmAa'

    # Test the Kafka connection
    test_kafka_connection(bootstrap_servers, username, password)
