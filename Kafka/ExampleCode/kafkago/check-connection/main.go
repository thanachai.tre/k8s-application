package main

import (
	"context"
	// "fmt"
	"crypto/tls"
	"log"
	"time"

	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/sasl/scram"
)

type KafkaConfig struct {
	Username string
	Password string
}

func main() {
	config := KafkaConfig{
		Username: "kafdrop-user",
		Password: "HXO3fMLo5kr0uGPyBgS7dTS3ht9CmmAa",
	}
	kafkaAddress := "kafka-external.kanomnutt.site:443"

	mechanism, err := scram.Mechanism(scram.SHA512, config.Username, config.Password)
	if err != nil {
		panic(err)
	}

	dialer := &kafka.Dialer{
		Timeout:       10 * time.Second,
		DualStack:     true,
		SASLMechanism: mechanism,
		TLS: &tls.Config{
			InsecureSkipVerify: true, // Set to false if you have a valid certificate
		},
	}

	conn, err := dialer.DialLeader(context.Background(), "tcp", kafkaAddress, "test-topic", 0)
	if err != nil {
		log.Fatalf("Failed to connect to Kafka: %v", err)
	}
	defer conn.Close()

	log.Println("Connected to Kafka successfully!")
}
