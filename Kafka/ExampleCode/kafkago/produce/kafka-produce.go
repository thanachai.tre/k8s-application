package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"time"

	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/sasl/scram"
)

type KafkaConfig struct {
	Username string
	Password string
}

func main() {
	config := KafkaConfig{
		Username: "kafdrop-user",
		Password: "HXO3fMLo5kr0uGPyBgS7dTS3ht9CmmAa",
	}
	kafkaAddress := "kafka-external.kanomnutt.site:443"
	focusTopic := "kafkatopic02"

	mechanism, err := scram.Mechanism(scram.SHA512, config.Username, config.Password)
	if err != nil {
		panic(err)
	}

	dialer := &kafka.Dialer{
		Timeout:   10 * time.Second,
		DualStack: true,
		TLS: &tls.Config{
			InsecureSkipVerify: true, // Set to false if you have a valid certificate
		},
		SASLMechanism: mechanism,
		// SASLMechanism is automatically set based on the mechanism used
	}

	writer := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{kafkaAddress},
		Topic:   focusTopic,
		Dialer:  dialer,
	})

	message := kafka.Message{
		Key:   []byte("2"),
		Value: []byte("Hello, world"),
	}

	ctx := context.Background()
	err = writer.WriteMessages(ctx, message)
	if err != nil {
		fmt.Printf("Error sending message: %v\n", err)
	}

	writer.Close()
	log.Printf("Message sent to Kafka: %s\n", message.Value)
}
