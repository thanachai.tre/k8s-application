# Kubernetes Adds-On External DNS
External DNS is a Kubernetes add-on that automatically updates DNS records for your Kubernetes resources. This allows you to expose your Kubernetes services to the outside world without having to manually manage DNS records.

## Prerequisite
-   Kuberbetes cluster
-   Domain Name with DNS provider that support Kubernetes External DNS [DNS Provider list](https://github.com/kubernetes-sigs/external-dns#status-of-providers)

## Installation
-   Create Kuberbetes Cluster Role to get, watch and list pod, service and ingresses resource for External DNS.
-   Create Kubernetes ServiceAccount and ClusterRolebinding.
-   Create External DNS Deployment and test create deployment with ingresss or service type: LoadBalancer [Guide line configuration](https://github.com/kubernetes-sigs/external-dns/tree/master/docs/tutorials).
-   Check A Record on DNS provider dashboard and try to access it.