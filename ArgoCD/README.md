# ArgoCD - Continuous Delivery for Kubernetes

## Install ArgoCD
-  run [install.sh](install.sh)
```bash
chmod +x install.sh
./install.sh
```
## Access Argocd CD Dashboard
The initial password for the admin account is auto-generated and stored as clear text in the field password in a secret named argocd-initial-admin-secret in your Argo CD installation namespace.

## Create ArgoCD Project
- Copy [Project-kafka.yaml](Project-kafka.yaml) to new file and change name of project.
- Run kubectl apply to create it.
```bash
kubectl apply -n argocd -f Project-kafka.yaml
```

## Create ArgoCD Application 
- Copy Application YAML file to a new file and change name of Application, destination namespace, project name, and Git information.
- Run kubectl apply to create it. 
```bash
kubectl apply -n argocd -f Application-kafkatopic.yaml
```
