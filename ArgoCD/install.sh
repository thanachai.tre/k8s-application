#!/bin/bash

kubectl create ns argocd
# Install ArgoCD
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
# Create SSL for Ingress
kubectl apply -n argocd -f Certificate-argocd.yaml
# Creat Ingress for ArgoCD
kubectl apply -n argocd -f Ingress-ArgoCD.yaml