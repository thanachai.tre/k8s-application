# K8s Application Repository

Greetings to the K8s Application Repository! Here, you'll discover a wealth of Kubernetes Application configurations and documentation. Our mission is to empower you to consolidate expertise and collaborate in knowledge-sharing. Let's embark on this Kubernetes journey together!
#

## Kubernetes Configuration Repository

This repository houses essential configuration files for various Kubernetes components utilized in our applications. The files are neatly organized by component and are stored in YAML format. You will find them categorized into the following folders:

### Cert-Manager
Contains configuration files for the cert-manager, a Kubernetes add-on that provides automated certificate management. [Cert-Manager](https://cert-manager.io/)

### Strimzi Kafka
Contains configuration files for Apache Kafka, a distributed streaming platform. [Strimzi Kafka](https://strimzi.io/)

### Nginx Ingress
Contains configuration files for the Nginx Ingress Controller, a Kubernetes ingress controller that provides load balancing and SSL termination for HTTP requests. [Nginx Ingress](https://github.com/kubernetes/ingress-nginx)

### Traefik
Contains configuration files for Traefik, a modern HTTP reverse proxy and load balancer. [Traefik Ingress](https://traefik.io/)

### TiDB
Contains configuration files for TiDB, a distributed relational database. [TiDB](https://www.pingcap.com/)

### Prometheus 
Contains configuration files for Prometheus-operator, a Kubernetes operator for Prometheus. [Prometheus](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)

### ECK
Contains configuration files for Elastic Cloud on Kubernetes (ECK), a managed Elasticsearch, Kibana, and Beats distribution for Kubernetes. [ECK](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-quickstart.html)

### ELK
Contains configuration files for the ELK stack, a popular logging and monitoring solution. [ELK](https://www.elastic.co/what-is/elk-stack)

### Storage Class
The storage class configuration files have been updated to include Portworx local LVM and OpenEBS-ZFS local pv. To use these storage classes, you will need to have the Portworx or OpenEBS storage providers installed in your Kubernetes cluster.

For more information on Portworx, please visit [PortworX](https://www.portworx.com/)

For more information on OpenEBS, please visit [OpenEBS](https://openebs.io/)
### Azure Key Vault
We have now introduced support for Azure Key Vault to securely store Kubernetes secrets. By utilizing Azure Key Vault, you can enhance the security of your sensitive data and ensure that critical information like passwords, API keys, and certificates are stored in a safe and centralized location.

### Kasten Backup 
Kasten is a powerful data management platform that helps you protect, secure, and manage your Kubernetes applications and persistent data. With Kasten, you can easily backup, restore, and migrate your containerized applications, ensuring that your data is safe, and your applications run smoothly in your Kubernetes environment. [Kasten](https://docs.kasten.io/latest/index.html)

### ArgoCD
Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes. [ArgoCD](https://argo-cd.readthedocs.io/en/stable/getting_started/)
#

## Usage
To use this repository, clone it to your local machine and navigate to the appropriate folder for the tool you want to use. Follow the instructions in the README file in each folder to deploy and manage infrastructure using IaC principles.
#

## Contribution
This repository is open to contributions from the community. If you have any ideas, bug reports, or improvements, feel free to open an issue or submit a pull request.
#
## License
This repository is licensed under the MIT License. See the [LICENSE](https://gitlab.com/thanachai.tre/thanachai-repo/-/blob/main/LICENSE) file for details.

